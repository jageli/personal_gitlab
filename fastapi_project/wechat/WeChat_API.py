from fastapi import APIRouter, Body, Query
from typing import List, Optional
from .main import WeChat
from .body import *

router = APIRouter()
myWeChat = WeChat()


@router.get(
    "/req_token",
    summary='请求Access token',
    description=
    'https请求方式: GET https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET'
)
async def admin():
    try:
        return myWeChat.req_token()
    except Exception as e:
        return str(e)


@router.get(
    "/get_token",
    summary='获取Access token',
    description=
    'https请求方式: GET https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET'
)
async def admin():
    try:
        return myWeChat.get_token()
    except Exception as e:
        return str(e)


@router.get(
    "/getuser",
    summary='获取用户列表',
    description=
    'http请求方式: GET（请使用https协议）https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID'
)
async def admin():
    try:
        return myWeChat.getuser()
    except Exception as e:
        return str(e)


@router.post(
    "/send_message",
    summary='客服接口-发消息',
    description=
    'http请求方式: POST https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN'
)
async def admin(SendMessageBody: dict = Body(..., example=SendMessageBody)):
    try:
        return myWeChat.send_message(SendMessageBody)
    except Exception as e:
        return str(e)


@router.post(
    "/template_message",
    summary='模板消息接口',
    description=
    'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN'
)
async def admin(template_message_body: dict = Body(
    ..., example=template_message_body)):
    try:
        return myWeChat.template_message(template_message_body)
    except Exception as e:
        return str(e)


@router.get(
    "/get_tag",
    summary='获取公众号已创建的标签',
    description=
    'http请求方式:GET（请使用https协议） https://api.weixin.qq.com/cgi-bin/tags/get?access_token=ACCESS_TOKEN'
)
async def admin():
    try:
        return myWeChat.get_tag()
    except Exception as e:
        return str(e)


@router.post(
    "/creat_tag",
    summary='创建标签',
    description=
    'http请求方式：POST（请使用https协议） https://api.weixin.qq.com/cgi-bin/tags/create?access_token=ACCESS_TOKEN'
)
async def admin(creat_tag_body: dict = Body(..., example=creat_tag_body)):
    try:
        return myWeChat.creat_tag(creat_tag_body)
    except Exception as e:
        return str(e)


@router.post(
    "/update_tag",
    summary=' 编辑标签',
    description=
    'http请求方式：POST（请使用https协议） https://api.weixin.qq.com/cgi-bin/tags/update?access_token=ACCESS_TOKEN'
)
async def admin(update_tag_body: dict = Body(..., example=update_tag_body)):
    try:
        return myWeChat.update_tag(update_tag_body)
    except Exception as e:
        return str(e)


@router.delete(
    "/del_tag",
    summary='删除标签',
    description=
    'http请求方式：POST（请使用https协议） https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=ACCESS_TOKEN'
)
async def admin(del_tag_body: dict = Body(..., example=del_tag_body)):
    try:
        return myWeChat.del_tag(del_tag_body)
    except Exception as e:
        return str(e)


@router.post(
    "/batchtagging",
    summary='批量为用户打标签',
    description=
    'http请求方式：POST（请使用https协议） https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=ACCESS_TOKEN'
)
async def admin(openid_list: Optional[List[str]] = Query(...),
                tagid: int = Query(101)):
    body = {"openid_list": openid_list, "tagid": tagid}
    try:
        return myWeChat.batchtagging(body)
    except Exception as e:
        return str(e)


@router.post(
    "/batchuntagging",
    summary='批量为用户取消标签',
    description=
    'http请求方式：POST（请使用https协议） https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=ACCESS_TOKEN'
)
async def admin(openid_list: Optional[List[str]] = Query(...),
                tagid: int = Query(101)):
    body = {"openid_list": openid_list, "tagid": tagid}
    try:
        return myWeChat.batchuntagging(body)
    except Exception as e:
        return str(e)


@router.post(
    "/getidlist",
    summary='获取用户身上的标签列表',
    description=
    'http请求方式：POST（请使用https协议） https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token=ACCESS_TOKEN'
)
async def admin(openid: str = Query(...)):
    body = {"openid": openid}
    try:
        return myWeChat.getidlist(body)
    except Exception as e:
        return str(e)


@router.post(
    "/sendall",
    summary='根据标签进行群发',
    description=
    'http请求方式: POST https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN'
)
async def admin():
    return myWeChat.sendall()
    # try:
    #     return myWeChat.sendall()
    # except Exception as e:
    #     return str(e)
