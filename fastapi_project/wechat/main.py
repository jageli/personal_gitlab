#!usr/bin/env python
# -*- coding:utf-8 -*-

import json
import requests
import time
import os


class WeChat(object):

    def __init__(self):
        self.appID = 'wx809a4f3000e1159d'
        self.appsecret = 'c9b08d3a20d1cd66c640abf2158d3ac4'
        self.proxies = {
            'https': 'http://10.144.1.10:8080',
            'http': 'http://10.144.1.10:8080'
        }
        self.pwd = os.getcwd()
        self.access_json = os.path.join(self.pwd, 'wechat/access_token.json')

    def sendall(self):
        url = f'https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={self.get_token()}'
        body = {
            "filter": {
                "is_to_all": False,
                "tag_id": 101
            },
            "text": {
                "content": "hello word\nwomendf\ndfsdfsd\nfdsfdsf"
            },
            "msgtype": "text"
        }
        res = requests.post(url=url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), proxies=self.proxies)
        return res.json()

    def getuser(self):
        url = f'https://api.weixin.qq.com/cgi-bin/user/get?access_token={self.get_token()}'
        # body = {"tag": {"id": 100}}
        res = requests.get(url=url, proxies=self.proxies)
        return res.json()

    def getidlist(self, body):
        url = f'https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token={self.get_token()}'
        # body = {"tag": {"id": 100}}
        res = requests.post(url=url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), proxies=self.proxies)
        return res.json()

    def batchuntagging(self, body):
        url = f'https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token={self.get_token()}'
        # body = {"tag": {"id": 100}}
        res = requests.post(url=url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), proxies=self.proxies)
        return res.json()

    def batchtagging(self, body):
        url = f'https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token={self.get_token()}'
        # body = {"tag": {"id": 100}}
        res = requests.post(url=url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), proxies=self.proxies)
        return res.json()

    def del_tag(self, body):
        url = f'https://api.weixin.qq.com/cgi-bin/tags/delete?access_token={self.get_token()}'
        # body = {"tag": {"id": 100}}
        res = requests.post(url=url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), proxies=self.proxies)
        return res.json()

    def update_tag(self, body):
        url = f'https://api.weixin.qq.com/cgi-bin/tags/update?access_token={self.get_token()}'
        # body = {"tag": {"id": 100, "name": "mya"}}
        res = requests.post(url=url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), proxies=self.proxies)
        return res.json()

    def creat_tag(self, body):
        url = f'https://api.weixin.qq.com/cgi-bin/tags/create?access_token={self.get_token()}'
        # body = {"tag": {"name": "my"}}
        res = requests.post(url=url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), proxies=self.proxies)
        return res.json()

    def get_tag(self):
        url = f'https://api.weixin.qq.com/cgi-bin/tags/get?access_token={self.get_token()}'
        res = requests.get(url=url, proxies=self.proxies)
        return res.json()

    def template_message(self, body):
        url = f'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={self.get_token()}'
        # body = {
        #     "touser": "omltu55EB0cnpB8aqeRGpGnSZb-Q",
        #     "template_id": "McB-RY2pXPZ1k_wysC6tdaB9O5Cq0TmLH3Gfv9SnIrU",
        #     "topcolor": "#DC143C",
        #     "data": {
        #         "title": {
        #             "value": "www.baidu.com",
        #             "color": "#173177"
        #         },
        #         "content": {
        #             "value": "www.baidu.com",
        #             "color": "#DC143C"
        #         }
        #     }
        #
        # }
        res = requests.post(url=url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), proxies=self.proxies)
        return res.json()

    def send_message(self, body):
        url = f'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={self.get_token()}'
        # body = {
        #     "touser": "omltu5_e5mVCaLnwTZsh514kSI64",
        #     "msgtype": "text",
        #     "text":
        #         {
        #             "content": "aaaaabbbbcccc"
        #         }
        # }
        res = requests.post(url=url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), proxies=self.proxies)
        return res.json()

    def req_token(self):
        with open(self.access_json, "w") as json_file:
            url = f'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={self.appID}&secret={self.appsecret}'
            resp = requests.get(url=url, proxies=self.proxies).json()
            json.dump(resp, json_file)
            access_token = resp.get('access_token')
            return access_token

    def get_token(self):
        try:
            with open(self.access_json, "r") as json_file:
                json_dict = json.load(json_file)
                expires_in = json_dict.get('expires_in')
                access_token = json_dict.get('access_token')
                mtime = os.path.getmtime(self.access_json)
                timeNow = time.time()
                if timeNow - mtime > expires_in:
                    access_token = self.req_token()
                    return access_token
                else:
                    return access_token
        except:
            access_token = self.req_token()
            return access_token


if __name__ == '__main__':
    myWeChat = WeChat()
    # print(myWeChat.get_token())
    # myWeChat.send_message()
    # myWeChat.template_message()
    # myWeChat.creat_tag()
    # myWeChat.get_tag()
    # myWeChat.update_tag()
    # myWeChat.del_tag()
    print(myWeChat.sendall())
