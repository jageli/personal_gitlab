import uvicorn
from fastapi import FastAPI
from VRAN.VRAN_API import router as VRAN_API
from wechat.WeChat_API import router as WeChat_API

app = FastAPI(
    title='FastAPI Admin',
    description='',
    version='v1',
    docs_url='/docs',
    redoc_url='/redocs',
)

app.include_router(VRAN_API,
                   prefix='',
                   tags=['vRAN2.0'],
                   include_in_schema=False)
app.include_router(WeChat_API, prefix='', tags=['WeChat'])

if __name__ == '__main__':
    uvicorn.run('run:app',
                host='127.0.0.1',
                port=8000,
                reload=True,
                debug=True,
                workers=1)
