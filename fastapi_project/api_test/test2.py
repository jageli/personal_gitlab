from fastapi import APIRouter

router = APIRouter()


@router.post(path='/api3')
def fun3():
    return 3


@router.post(path='/api4')
def fun4():
    return 4
