from fastapi import APIRouter

router = APIRouter()


@router.post(path='/api1')
def fun1():
    return 1


@router.post(path='/api2')
def fun2():
    return 2
