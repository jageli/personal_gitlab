from fastapi import APIRouter, Body
import re
from enum import Enum
from .VRAN import FastActivations, vdu_state_data

router = APIRouter()

Example = [
    {
        "className": "TRBLCADM",
        "distName": "MRBTS-608/MNL-1/MNLENT-1/TRBLCADM-1",
        "operationType": "update",
        "payload": "{\"actDefaultFaultSnapTriggers\":false}"
    }
]


class SCF_exportType_vran(str, Enum):
    configuration = "Save SCF - configuration only"
    configurationAndRuntimeObjects = "Save full runtime configuration"


def check_ip(ip):
    if ip.strip():
        if (re.match(
                r"^(25[0-5]|2[0-4][0-9]|[1][0-9]{2}|[1-9][0-9]|[1-9])(\.(25[0-5]|2[0-4][0-9]|[1][0-9]{2}|[1-9][0-9]|[0-9])){3}$",
                ip)):
            return True
        else:
            raise Exception('ip地址错误')


@router.post("/delta-plans/operations/fast-activations", summary=' ')
async def admin(ip: str, item: list = Body(..., example=Example, )):
    try:
        check_ip(ip)
        return FastActivations(ip=ip, data=item)
    except Exception as e:
        return str(e)


@router.post("/v2/current-plan/operations/uploads", summary='get scf')
async def admin(ip: str, SCF_exportType: SCF_exportType_vran):
    # return SCF_exportType.value
    try:
        check_ip(ip)
        includeRuntimeData = 'false'
        if 'runtime' in SCF_exportType.value:
            includeRuntimeData = 'true'

        return SCF_exportType.value
    except Exception as e:
        return str(e)


@router.get("/vdu", summary='get vDU state data')
async def admin(ip: str):
    try:
        check_ip(ip)
        return vdu_state_data(ip=ip)
    except Exception as e:
        return str(e)
