import requests
import json
import time


class VRan(object):
    def __init__(self, ip='192.168.255.129', data=None):
        self.ip = ip
        self.data = data
        self.Authorization = 'Basic TmVtdWFkbWluOm5lbXV1c2Vy'
        self.UserAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36'
        self.session = requests.session()
        self.headers = {
            'Authorization': self.Authorization,
            'User-Agent': self.UserAgent
        }

    def login(self):
        # 登陆VDU/VCU
        url = f'https://{self.ip}/api/sec/v1/login'
        response = self.session.post(url, headers=self.headers, verify=False)
        print(response)
        Token = response.json().get('Token')
        self.headers['Authorization'] = f'Bearer {Token}'

    def close(self):
        self.session.close()

    def vdu_state_data(self):
        url = f'https://{self.ip}/api/licensed/v1/vdu'
        response = self.session.get(url, headers=self.headers, verify=False)
        return response.json()

    def FastActivations(self):
        url = f'https://{self.ip}/api/asm/v1/delta-plans/operations/fast-activations'
        response = self.session.post(url, headers=self.headers, data=json.dumps(self.data), verify=False)
        # print(response.text)
        ID = response.json().get('id')
        url1 = url + '/' + ID
        while True:
            stat = self.session.get(url1, headers=self.headers, verify=False)
            status = stat.json().get('status')
            print(status)
            if status != 'ongoing':
                print(status)
                break
            time.sleep(1)
        return status

    def get_SCF(self):
        url = f'https://{self.ip}/api/cm/v2/current-plan/operations/uploads'


def FastActivations(ip=None, data=None):
    myVRAN = VRan(ip, data)
    myVRAN.login()
    status = myVRAN.FastActivations()
    myVRAN.close()
    return status


def vdu_state_data(ip=None):
    myVRAN = VRan(ip)
    myVRAN.login()
    resp = myVRAN.vdu_state_data()
    myVRAN.close()
    return resp


if __name__ == "__main__":
    vdu_state_data(ip='10.64.161.130')

#     data = [
#         {
#             "className": "TRBLCADM",
#             "distName": "MRBTS-608/MNL-1/MNLENT-1/TRBLCADM-1",
#             "operationType": "update",
#             "payload": "{\"actDefaultFaultSnapTriggers\":false}"
#         }
#     ]
#     aa = FastActivations(ip='10.64.161.130', data=data)
#     print(aa)
