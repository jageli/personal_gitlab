#!usr/bin/env python
# -*- coding:utf-8 -*-
"""
#!usr/bin/env python
#-*- coding:utf-8 -*-
"""
import json
import requests

url_token = 'https://api.weixin.qq.com/cgi-bin/token?'
proxies = {
    'https': 'http://10.144.1.10:8080',
    'http': 'http://10.144.1.10:8080'
}
params = {

    "grant_type": 'client_credential',
    'appid': 'wx809a4f3000e1159d',  # 这里填写上面获取到的appID
    'secret': 'c9b08d3a20d1cd66c640abf2158d3ac4',  # 这里填写上面获取到的appsecret
}
res = requests.get(url=url_token, params=params, proxies=proxies)
token = res.json()

print(token)
#
# body = {
#
#     "touser": 'OPENID',  # 这里必须是关注公众号测试账号后的用户id
#     "msgtype": "text",
#     "text": {
#
#         "content": "Hello from python. Any problem, email chaj@hit.edu.cn"
#     }
# }

# token = '55_O5jg5OyjHHHw0Qs6ugI9a0FXZVRPU7jxOJLbaytMXgtZcgAXI_NZ92zI8tvpwPIng62k655A8cPzpge9bSnunPI-BAbV2nruyXf-RG6FH5ZFr0kASlxNvw63MSW6xTF-54196o7tHXCgry3FQAFhACAZJO'
params = {
    'access_token': token

}

# url_msg = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?'
# res = requests.post(url=url_msg, params=params, data=json.dumps(body, ensure_ascii=False).encode('utf-8'),
#                     proxies=proxies)

# url = 'https://api.weixin.qq.com/customservice/kfaccount/add?'
# body = {
#     "kf_account": "test1@test",
#     "nickname": "客服1",
#     "password": "pswmd5"
# }
# res = requests.post(url=url, params=params, data=json.dumps(body, ensure_ascii=False).encode('utf-8'),
#                     proxies=proxies)
# print(res.json())

# url = 'https://api.weixin.qq.com/cgi-bin/customservice/getkflist?'
# res = requests.get(url=url, params=params, proxies=proxies)
# print(res.json())
#
url = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?'
body = {
    "touser": "omltu55EB0cnpB8aqeRGpGnSZb-Q",
    "msgtype": "text",
    "text":
        {
            "content": "Hello World"
        }
}
res = requests.post(url=url, params=params, data=json.dumps(body, ensure_ascii=False).encode('utf-8'),
                    proxies=proxies)
print(res.json())
